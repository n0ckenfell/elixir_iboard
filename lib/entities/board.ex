defmodule IBoard.Entities.Board do
  use  Repox.Entity.Macros

  defstruct id: nil, name: "", topics: []

  def first do
    %IBoard.Entities.Board{id: :new, name: "(unnamed)"}
  end

  def topic_subjects board do
    Enum.map board.topics, fn(topic) ->
      topic.subject
    end
  end

  # REWORK: Use a template-file or module to render the output
  def to_ascii board do
    lines(board) |> Enum.join("\n" )
  end

  defp lines board do
    [
      String.upcase( board.name ),
      "",
      "Topics:" | board.topics |> list_topics
    ]
  end

  defp list_topics topics do
    Enum.map(topics,&print_topic/1)
  end

  defp print_topic(topic) do
    "  - " <> Enum.join([topic.subject | list_items(topic_items(topic)) ],"\n")
  end

  defp topic_items topic do
    case Map.has_key?(topic, :items) do
      true  -> topic.items
      false -> []
    end
  end

  defp list_items items do
    Enum.map(items, fn(item) ->
      "    - " <> item.subject
    end)
  end
end


