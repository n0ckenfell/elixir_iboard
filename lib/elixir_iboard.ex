defmodule IBoard do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    IO.puts "START APPLICATION iBOARD FROM #{__ENV__.file}"
    children = [
      worker(IBoard.Stash,             [ IBoard.Stash.defaults ]),
      worker(IBoard.Repository.Boards, [ [] ])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: IBoard.Supervisor]
    Supervisor.start_link(children, opts)
  end

  #
  # PUBLIC API
  #
end
