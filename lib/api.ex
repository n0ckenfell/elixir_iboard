defmodule IBoard.Stash.API do
  require IBoard.Stash

  def stash key, value do
    IBoard.Stash.push key, value
  end

  def stash key do
    IBoard.Stash.get key
  end

  def stash do
    IBoard.Stash.state
  end

end

