defmodule IBoard.Stash do
  use GenServer

  def defaults do
    [ { :started_at, :os.timestamp } ]
  end

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, [name: :iboard_stash])
  end

  def state do
    GenServer.call(:iboard_stash, :get_state)
  end

  def keys do
    GenServer.call(:iboard_stash, :get_state)
      |> Enum.map fn(entry) ->
        { key, _value } = entry
        key
      end
  end

  def push key, value do
    GenServer.cast(:iboard_stash, {:push, key, value})
  end

  def get key do
    GenServer.call(:iboard_stash, {:get, key})
  end

  # GenServe Callbacks

  def handle_call(:get_state,_from, state) do
    {:reply, state, state}
  end

  def handle_call({:get, key},_from, state) do
    {:reply, state[key], state}
  end

  def handle_cast({:push,key,value}, state) do
    {:noreply, [{key, value} | state]}
  end
end
