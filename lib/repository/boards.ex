defmodule IBoard.Repository.Boards do
  use GenServer

  # Public API

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, [name: :iboard_boards])
  end

  def first do
    GenServer.call(:iboard_boards, :first)
  end

  def add board do
    GenServer.cast(:iboard_boards, {:add, board})
    board
  end

  def index do
    GenServer.call(:iboard_boards, :index)
  end

  def find id do
    GenServer.call(:iboard_boards, { :find_by_id, id } )
  end

  def drop! do
    GenServer.cast(:iboard_boards, :drop)
  end

  # GenServer Callbacks

  def handle_cast({:add, entry}, state) do
    {:noreply, [entry|state]}
  end

  def handle_call(:index, _from, state) do
    {:reply, state, state}
  end

  def handle_call(:first, _from, state) do
    [first|_rest] = state
    {:reply, first, state}
  end

  def handle_call {:find_by_id, id}, _from, state do
    board = Enum.find(state, {:error, %{error: "not found"}}, fn(b) ->
      b.id == id
    end)
    {:reply, board, state}
  end

  def handle_cast(:drop, _state) do
    {:noreply, []}
  end
end
