# Test Draft Elixir iBoard

## Subject

iBoard is a simple Pages/Blog scaffold supposed to be used to build
individual applications apon it.

### Brainstorm items are

  - Page
  - CurrentUser, NilUser
  - Comment (By user on page)
    - polymorphic on pages, users, and recursively on comments
  - Attachment
    - polymorphic on pages, users, and comments
