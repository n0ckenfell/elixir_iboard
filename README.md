Elixir IBoard
=============

## Linked Projects

  * [repox] Elixir Repository Pattern with InMemory, File, and MongoDB
    Gateway.
  * [elixir_iboard] Defines a "Boards Repository", using Repox.
  * [iboard_phoenix] Phoenix Web-project using [elixir_iboard]

[iboard_phoenix]: https://bitbucket.org/n0ckenfell/iboard_phoenix/overview
[elixir_iboard]:  https://bitbucket.org/n0ckenfell/elixir_iboard
[repox]:          https://github.com/iboard/repox

