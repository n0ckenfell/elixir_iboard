defmodule IBoard.Repository.BoardsTest do
  use    ExUnit.Case
  import TestHelper

  alias  IBoard.Repository.Boards
  alias  IBoard.Entities.Board

  @fixtures_file "#{__DIR__}/fixtures/boards.json"

  setup do
    IBoard.Repository.Boards.drop!
    {:ok,
      fixtures: load_fixtures(:boards,@fixtures_file),
      subject:  Boards.index,
      board:    Boards.first,
      expected: %Board{
                  id:     1,
                  name:   "Welcome Board",
                  topics: [
                    %{ id: 1, subject: "Welcome",
                       description: "Thank you!",
                       items: [
                         %{id: 1, subject: "What to expect", body: "Lorem ..."},
                         %{id: 2, subject: "What to know", body: "Lorem ..."},
                       ]},
                    %{ id: 2, subject: "What's next" },
                    %{ id: 3, subject: "Unhappy?" }
                  ]
                }
    }
  end

  test "Boards.index has all loaded fixtures", %{subject: boards, fixtures: f} do
    assert boards == Enum.reverse(f)
  end

  test "Boards.first has the expected Board-structure", %{expected: expected} do
    assert expected == Boards.first
  end

  test "The Welcome Board has three topics", %{board: b} do
    assert b.name == "Welcome Board"
    assert Board.topic_subjects(b) == ["Welcome", "What's next", "Unhappy?" ]
  end

  test "Board.to_ascii renders expected text including topics, and subjects",
    %{board: b} do

    assert Board.to_ascii(b) <> "\n" == """
    WELCOME BOARD

    Topics:
      - Welcome
        - What to expect
        - What to know
      - What's next
      - Unhappy?
    """
  end

  test "Boards.find(id) returns the Board found" do
    board = Boards.find(2)
    assert "First Steps" == board.name
  end

  test "Board.find(id) retruns an errero if not found" do
    expected = {:error, %{ error: "not found"}}
    board = Boards.find(:not_existing)
    assert board == expected
  end

end
