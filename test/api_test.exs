defmodule IBoard.APITest do
  use ExUnit.Case

  # Global, Supervised Servers are running

  test "Access application stash sets started_at-date" do
    started_at = IBoard.Stash.get :started_at
    assert {_a,_b,_c} = started_at
  end

  test "Save and retreive values from IBoard.Stash" do
    IBoard.Stash.API.stash :new_key, "New Value"
    assert IBoard.Stash.API.stash(:new_key) == "New Value"
  end
end
