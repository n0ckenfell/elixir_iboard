ExUnit.start()

defmodule TestHelper do
  defmacro pp what do
    quote do
      IO.puts inspect unquote(what), [pretty: true]
    end
  end

  def load_fixtures(:boards, file) do
    File.read!(file)
    |> Poison.Parser.parse!(keys: :atoms!)
    |> Enum.reverse |> Enum.map fn(board) ->
      Map.merge(%IBoard.Entities.Board{},board)
      |> IBoard.Repository.Boards.add
    end
  end
end
